package org.els.ingest.data;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParentChildGenerator {

    final static String indexName = "test_data_parent";

    final static RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
            RestClient.builder(new HttpHost("127.0.0.1", 443, "https"))
                    .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder
                            .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE))
                    .setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
                        @Override
                        public RequestConfig.Builder customizeRequestConfig(
                                RequestConfig.Builder requestConfigBuilder) {
                            return requestConfigBuilder
                                    .setConnectTimeout(50000)
                                    .setSocketTimeout(30000);
                        }
                    }));

    public static void main(String[] args) throws IOException {
        try {
            BulkRequest bulkRequest = new BulkRequest();
            final Random random = new Random();

            List<AnalogSignal> generatedSignals = generateAnalogSignals(random, 1613506550000L, 10);

            for (AnalogSignal analogSignal : generatedSignals) {
                IndexRequest childRequest = buildChildDoc(analogSignal, analogSignal.timeValues.stream().min(Long::compare).get(), "V_1" + "_" + "S_1" + "_" + 1613506550000L);
                System.out.println(childRequest.source().utf8ToString());
                bulkRequest.add(childRequest);
            }

            IndexRequest indexRequest = buildParentDoc(1613506550000L, "V_1", "S_1");
            bulkRequest.add(indexRequest);
            System.out.println(indexRequest.source().utf8ToString());


            BulkResponse bulkItemResponses = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

            for (BulkItemResponse response : bulkItemResponses.getItems()) {
                System.out.println(response.status().getStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            restHighLevelClient.close();
        }
    }

    public static List<AnalogSignal> generateAnalogSignals(final Random random, final Long date, long number) {
        int max = 50;
        int min = 26;

        return Stream.generate(() -> random.nextInt((max - min) + 1) + min).limit(number).map(s -> {
            AnalogSignal analogSignal = new AnalogSignal();
            analogSignal.identifier = "A_" + s;
            Stream.generate(() -> random.nextInt((max - min) + 1) + min).limit(900)
                    .collect(Collectors.toList())
                    .forEach(v -> {
                        analogSignal.timeValues.add(date + 200);
                        analogSignal.doubleValues.add(v.doubleValue());
                    });

            return analogSignal;
        }).collect(Collectors.toList());
    }

    public static IndexRequest buildParentDoc(long time, String vehicleId, String serialNumber) throws IOException {
        final XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        builder.field("time", time);
        builder.field("vehicle_id", vehicleId);
        builder.field("serial_number", serialNumber);
        builder.startObject("relation_type");
        builder.field("name", "vehicle");
        builder.endObject();
        builder.endObject();

        return new IndexRequest(indexName).id(String.valueOf(vehicleId + "_" + serialNumber + "_" + time)).source(builder);
    }

    public static IndexRequest buildChildDoc(AnalogSignal analogSignal, long time, String routing) throws IOException {
        final XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        builder.field("identifier", analogSignal.identifier);
        builder.field("time", time);
        builder.field("doubleValues", analogSignal.doubleValues.toArray());
        builder.field("timeValues", analogSignal.timeValues.toArray());
        builder.startObject("values_range");
        builder.field("gte", analogSignal.doubleValues.stream().min(Double::compare).get());
        builder.field("lte", analogSignal.doubleValues.stream().max(Double::compare).get());
        builder.endObject();
        builder.startObject("time_frame");
        builder.field("gte", analogSignal.timeValues.stream().min(Long::compare).get());
        builder.field("lte", analogSignal.timeValues.stream().max(Long::compare).get());
        builder.endObject();
        builder.startObject("relation_type");
        builder.field("name", "signals");
        builder.field("parent", routing);
        builder.endObject();
        builder.endObject();
        return new IndexRequest(indexName).source(builder).routing(routing);
    }
}