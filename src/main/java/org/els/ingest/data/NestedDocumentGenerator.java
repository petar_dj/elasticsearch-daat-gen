package org.els.ingest.data;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NestedDocumentGenerator {

    final static String indexName = "test_data";
    final static RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
            RestClient.builder(new HttpHost("127.0.0.1", 443, "https"))
                    .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder
                            .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE))
                    .setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
                        @Override
                        public RequestConfig.Builder customizeRequestConfig(
                                RequestConfig.Builder requestConfigBuilder) {
                            return requestConfigBuilder
                                    .setConnectTimeout(50000)
                                    .setSocketTimeout(30000);
                        }
                    }));

    public static void main(String[] args) throws IOException {
        try {
            AtomicLong atomicLong = new AtomicLong(1613506550000L);
            final Random random = new Random();
            List<String> listOfVehicles = Arrays.asList("V_1", "V_2", "V_3", "V_4", "V_5", "V_6", "V_7", "V_8", "V_9", "V_10");

            //Generate data for the list of vehicles
            AtomicReference<BulkRequest> requests = new AtomicReference<>(new BulkRequest());
            Stream.generate(() -> atomicLong.getAndAdd(180000)).limit(100)
                    .forEach(t -> {
                        listOfVehicles.forEach(v -> {
                            List<AnalogSignal> generatedSignals = generateAnalogSignals(random, t, 10);

                            Long minTime = generatedSignals.stream().flatMap(s -> s.timeValues.stream()).min(Long::compare).get();
                            Long maxTime = generatedSignals.stream().flatMap(s -> s.timeValues.stream()).max(Long::compare).get();

                            IndexRequest indexRequest = null;
                            try {
                                indexRequest = buildParentNestedDoc(t, v, "S_1", minTime, maxTime, generatedSignals);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            if (Objects.nonNull(indexRequest)) {
                                requests.get().add(indexRequest);
                            } else {
                                System.err.println("Something wrong requests are null");
                            }
                            //Manipulate with this
                            if (requests.get().numberOfActions() > 100) {
                                try {
                                    BulkResponse response = restHighLevelClient.bulk(requests.get(), RequestOptions.DEFAULT);
                                    response.forEach(r -> {
                                        System.out.println(r.status().getStatus());
                                        System.out.println(r.getId());
                                    });
                                    requests.set(new BulkRequest());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    });

            if (requests.get().numberOfActions() > 0) {
                BulkResponse response = restHighLevelClient.bulk(requests.get(), RequestOptions.DEFAULT);

                response.forEach(r -> {
                    System.out.println();
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            restHighLevelClient.close();
        }
    }

    public static List<AnalogSignal> generateAnalogSignals(final Random random, final Long date, long number) {
        int max = 50;
        int min = 26;

        return Stream.generate(() -> random.nextInt((max - min) + 1) + min).limit(number).map(s -> {
            AnalogSignal analogSignal = new AnalogSignal();
            analogSignal.identifier = "A_" + s;
            Stream.generate(() -> random.nextInt((max - min) + 1) + min).limit(90)
                    .collect(Collectors.toList())
                    .forEach(v -> {
                        analogSignal.timeValues.add(date + 200);
                        analogSignal.doubleValues.add(v.doubleValue());
                    });

            return analogSignal;
        }).collect(Collectors.toList());
    }

    public static IndexRequest buildParentNestedDoc(long time, String vehicleId, String serialNumber, long start, long end, List<AnalogSignal> analogSignals) throws IOException {
        final XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        builder.field("time", time);
        builder.field("vehicle_id", vehicleId);
        builder.field("serial_number", serialNumber);
        builder.startObject("time_frame");
        builder.field("gte", start);
        builder.field("lte", end);
        builder.endObject();
        builder.startArray("signals");

        for (AnalogSignal analogSignal : analogSignals) {
            builder.startObject();
            builder.field("identifier", analogSignal.identifier);
            builder.field("doubleValues", analogSignal.doubleValues.toArray());
            builder.field("timeValues", analogSignal.timeValues.toArray());
            builder.startObject("values_range");
            builder.field("gte", analogSignal.doubleValues.stream().min(Double::compare).get());
            builder.field("lte", analogSignal.doubleValues.stream().max(Double::compare).get());
            builder.endObject();
            builder.startObject("time_frame");
            builder.field("gte", analogSignal.timeValues.stream().min(Long::compare).get());
            builder.field("lte", analogSignal.timeValues.stream().max(Long::compare).get());
            builder.endObject();
            builder.endObject();
        }
        builder.endArray();
        builder.endObject();

        return new IndexRequest(indexName).id(String.valueOf(time)).source(builder);
    }
}

class AnalogSignal {
    String identifier;
    public double min;
    public double max;
    public LinkedList<Double> doubleValues = new LinkedList<>();
    public LinkedList<Long> timeValues = new LinkedList<>();
}